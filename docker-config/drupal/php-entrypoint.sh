#!/usr/bin/env sh

#   This entrypoint will be executed on container start.
#   If this file is updated, make sure you re-run "make install" or "docker-compose build"

DIR=.

if [ "$(ls -A $DIR)" ]; then
  echo "Take action $DIR is not Empty"
else
  composer create-project drupal/recommended-project $DIR
  mkdir web/sites/default/files/translations -p
  chmod -R 777 web/sites/default/files
  cp web/sites/default/default.settings.php web/sites/default/settings.php
  chmod -R 777 web/sites/default/settings.php
  composer install drush/drush

  #   Give all curent access for user
  chown 1000:1000 $DIR -R
fi

exec docker-php-entrypoint "$@"
