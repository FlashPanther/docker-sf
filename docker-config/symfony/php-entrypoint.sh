#!/usr/bin/env sh

#   This entrypoint will be executed on container start.
#   If this file is updated, make sure you re-run "make install" or "docker-compose build"

DIR=.

if [ "$(ls -A $DIR)" ]; then
  echo "Take action $DIR is not Empty"
else
  git config --global user.email "devos.thom@gmail.com"
  git config --global user.name "Thomas Devos"
  symfony new $DIR --webapp

  #   Give all curent access for user
  chown 1000:1000 $DIR -R
fi

exec docker-php-entrypoint "$@"
