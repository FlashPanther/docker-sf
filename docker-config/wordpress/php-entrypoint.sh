#!/usr/bin/env sh

#   This entrypoint will be executed on container start.
#   If this file is updated, make sure you re-run "make install" or "docker-compose build"

DIR=.

if [ "$(ls -A $DIR)" ]; then
  echo "Take action $DIR is not Empty"
else
  wget https://wordpress.org/latest.zip -P /tmp
  unzip /tmp/latest.zip -d /tmp
  cp -r /tmp/wordpress ./public
  cp public/wp-config-sample.php public/wp-config.php
  sed -i "s/database_name_here/$DATABASE_NAME/g" public/wp-config.php
  sed -i "s/username_here/root/g" public/wp-config.php
  sed -i "s/password_here/password/g" public/wp-config.php
  sed -i "s/localhost/db/g" public/wp-config.php

  #   Give all curent access for user
  chown 1000:1000 $DIR -R
fi

exec docker-php-entrypoint "$@"
