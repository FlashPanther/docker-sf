-include .env
all:
	docker-compose -f docker-config/$(PROJECT_TYPE)/docker-compose.yml up --force-recreate -d

install: clean
ifndef name
	$(error Project doesn't have a name)
endif
	@echo "PROJECT_NAME=$(name)\nDATABASE_NAME=$(name)\nPROJECT_TYPE=$(type)\nPROJECT_PORT=$(port)" > .env
	cp -r docker-config/$(PROJECT_TYPE)/docker-compose.yml .
	cp -r docker-config/$(PROJECT_TYPE)/Dockerfile .
	cp -r docker-config/$(PROJECT_TYPE)/nginx.conf .
	cp -r docker-config/$(PROJECT_TYPE)/php-entrypoint.sh . 2>/dev/null || :
	docker-compose build
	docker-compose up --force-recreate -d
	docker-compose logs -f

clean:
ifneq (,$(wildcard ./docker-compose.yml))
	docker-compose rm -sv
endif
	@rm www/ .env docker-compose.yml Dockerfile nginx.conf php-entrypoint.sh -rf
	@mkdir www
